#!/usr/bin/env python

import xmlrpclib, pickle
from xmlrpclib import Binary

def main():
  connection = xmlrpclib.ServerProxy("http://localhost:21234/")
  connection.terminate()
  print 'Server 21234 terminated unexpectedly!!'

if __name__ == "__main__":
  main()
