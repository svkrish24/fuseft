#!/usr/bin/env python

import xmlrpclib, pickle
from xmlrpclib import Binary

def main():
  connection = xmlrpclib.ServerProxy("http://localhost:21234/")
  corrupted_data = connection.corrupt('/file1')
  print 'Corrupting Data in File1 with', corrupted_data

if __name__ == "__main__":
  main()
