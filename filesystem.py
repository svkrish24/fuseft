#!/usr/bin/env python

import logging, xmlrpclib, pickle, errno

from collections import defaultdict
from errno import ENOENT
from stat import S_IFDIR, S_IFLNK, S_IFREG
from sys import argv, exit
from time import time
from xmlrpclib import Binary

from fuse import FUSE, FuseOSError, Operations, LoggingMixIn


if not hasattr(__builtins__, 'bytes'):
    bytes = str

class Server: 

    def __init__(self, Qr, Qw, meta_port, data_ports):
        self.Qr = Qr
        self.Qw = Qw
	self.connections = []
        self.connections.insert(0, (meta_port, xmlrpclib.ServerProxy("http://localhost:"+meta_port+"/"))) # Proxy is a server instance that connects to the Server simpleht.py 
        for i in data_ports:
            self.connections.append((i, xmlrpclib.ServerProxy("http://localhost:"+i+"/")))

    def checkAndSignalForErrorCorrection(self, key, dataRead, majorityData):
        print "Inside signalForEC"
        print 'DataRead', dataRead
        for data in dataRead.items():
            print 'Data in the list', data
            print 'Majority Data', majorityData
            if data[-1] != majorityData:
                print 'Server port', data[0]
                xmlrpclib.ServerProxy("http://localhost:"+data[0]+"/").correctError(key)
                print 'Error correction done'
        return   

    def checkForMatch(self, dataRead, Qr):
        print 'Inside checkformatch'
        dataList = dataRead.values()
        for data in dataList:
            if dataList.count(data) >= Qr:   
                print data 
	        return data
        print 'no match'
        return None    

    def putData(self, key, value, server_type='meta_server'):		# client stores the data in server as a key value pair where key is the file node's path name and the value can be file/directory metadata or the file contents
        if server_type == 'meta_server':
            self.connections[0][-1].put(Binary(str(key)), Binary(pickle.dumps(value)), 6000)
        if server_type == 'data_server':
	    i = 1
            print 'I before is', i
            print 'Qw is', self.Qw
	    while (i <= int(self.Qw)):
                print 'I is', i
                self.connections[i][-1].put(Binary(str(key)), Binary(pickle.dumps(value)), 6000)
		i += 1
          
	return

    def getData(self, key, server_type='meta_server'):		# client fetches the file node from server if its present
        if server_type == 'meta_server':
            try:
	        return pickle.loads(self.connections[0][-1].get(Binary(str(key)))['value'].data)
	    except KeyError:
                print "File does not exist"
                return ''
        if server_type == 'data_server':
            dataRead = {}
            for server_connection in self.connections[1:]:
                print 'Inside connection for loop'
                try:
                    print 'Inside try block'
	            dataRead[server_connection[0]] = pickle.loads(server_connection[-1].get(Binary(str(key)))['value'].data)
                except KeyError:
                    print 'KeyError'
                    continue
                except:
                    print 'CONNECTION ERROR OR SOME OTHER ERROR'
                    continue
            agreed_data = self.checkForMatch(dataRead, int(self.Qr))
            if agreed_data != None:
                try:
                    self.checkAndSignalForErrorCorrection(str(key), dataRead, agreed_data)
                except:
                    print 'inside the data corr except'
                print 'READ SUCCESSFUL'
                return str(agreed_data)
	    print 'READ UNSUCCESSFUL'
            return ''	    # what to return if read fails

            
    def removeData(self, key, server_type='meta_server'):		# client removes the file node from server
        if server_type == 'meta_server':
            self.connections[0][-1].put(Binary(str(key)), Binary(pickle.dumps('')), 0)
        if server_type == 'data_server':
	    i = 1
	    while (i <= int(self.Qw)):
                self.connections[i][-1].put(Binary(str(key)), Binary(pickle.dumps('')), 0)
		i += 1

	return


class Memory(LoggingMixIn, Operations):   #Hierarchical file system which stores its file contents and directories as individual file nodes in server

    def __init__(self, Qr, Qw, meta_port, data_ports):
        self.files = {}
        self.data = defaultdict(bytes)
        self.fd = 0
        now = time()
        self.files['/'] = dict(st_mode=(S_IFDIR | 0755), st_ctime=now, st_mtime=now, st_atime=now, st_nlink=2)
        self.server = Server(Qr, Qw, meta_port, data_ports)
	self.server.putData('/', self.files, 'meta_server') #might require change...what if server does not close nd client restarts


    def movContent(self, dirNode, oldPath, newPath):  # This function is used to change the absolute path name of all child files and directories with which they are stored when the parent directory is moved or renamed
	for x in dirNode: 
		if x.find('/') == -1:
			childDir = self.server.getData(oldPath+'/'+x)
			self.server.putData(newPath+'/'+x, childDir)
			self.movContent(childDir, oldPath+'/'+x, newPath+'/'+x)
			self.server.removeData(oldPath+'/'+x)
		if x.count('/') == 2:
			childFile = self.server.getData(oldPath+'/'+x.split('/')[-1])
			self.server.putData(newPath+'/'+x.split('/')[-1], childFile)
			self.server.removeData(oldPath+'/'+x.split('/')[-1])
			dataRead = self.server.getData(oldPath+'/'+x.split('/')[-1], 'data_server')
			self.server.putData(newPath+'/'+x.split('/')[-1], dataRead, 'data_server')
			self.server.removeData(oldPath+'/'+x.split('/')[-1], 'data_server')	
	
	return		


    def chmod(self, path, mode):
	pathList = path.split('/')			
	fsNode = self.server.getData(path)
	if '/'+pathList[-1] in fsNode:
 		fsNode['/'+pathList[-1]]['st_mode'] &= 0770000
       		fsNode['/'+pathList[-1]]['st_mode'] |= mode
	else:
        	fsNode['st_mode'] &= 0770000
        	fsNode['st_mode'] |= mode
	self.server.putData(path, fsNode, 'meta_server')		
	print 'chmod executed..'
        return 0

    def chown(self, path, uid, gid):
	pathList = path.split('/')			
	fsNode = self.server.getData(path)
	if '/'+pathList[-1] in fsNode:
		fsNode['/'+pathList[-1]]['st_uid'] = uid
        	fsNode['/'+pathList[-1]]['st_gid'] = gid
	else:
        	fsNode['st_uid'] = uid
        	fsNode['st_gid'] = gid		
	self.server.putData(path, fsNode)

    def create(self, path, mode):
	pathList = path.split('/')
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = self.server.getData(pathCwd)
	print 'cwd is', cwd
	fileName = '/'+pathList[-2]+'/'+pathList[-1]
	cwd[fileName] = ''
	newFile = dict(st_mode=(S_IFREG | mode), st_nlink=1, st_size=0, st_ctime=time(), st_mtime=time(), st_atime=time())
	self.server.putData(pathCwd, cwd)
	self.server.putData(path, newFile)
        self.server.putData(path, '', 'data_server')	
	print 'cwd now is', cwd
        self.fd += 1
	print 'RETURN VALUE', self.fd
	print 'create executed..'
        return self.fd

    def getattr(self, path, fh=None):
	print 'PATH is ', path
	if path == '/':
		cwd = self.server.getData('/')
		print 'cwd is', cwd
		print 'RETURN VALUE', cwd[path]
		print 'getattr executed..'
		return cwd[path]		
	pathList = path.split('/')
	fileName = '/'+pathList[-2]+'/'+pathList[-1]
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = self.server.getData(pathCwd)
	if pathList[-1] not in cwd and fileName not in cwd:
		print 'This is d pbm'
                raise FuseOSError(ENOENT)
	fsNode = self.server.getData(path)
	print 'fsNode is', fsNode
	if fileName in cwd:
		print 'FileName', fileName
		print 'RETURN VALUE', fsNode
		print 'getattr executed..'
        	return fsNode
	print 'RETURN VALUE', fsNode['/'+pathList[-1]]
	print 'getattr executed..'
        return fsNode['/'+pathList[-1]]

    def getxattr(self, path, name, position=0):	
	pathList = path.split('/')
	fsNode = self.server.getData(path)
	if '/'+pathList[-1] in fsNode:
		attrs = fsNode['/'+pathList[-1]].get('attrs', {})
	else:		
        	attrs = fsNode.get('attrs', {})

        try:
	    print 'RETURN VALUE', attrs[name]
	    print 'Path is ', path
	    print 'getxattr executed..'
            return attrs[name]
        except KeyError:
	    print 'getxattr keyerror executed..'
            return ''       # Should return ENOATTR

    def listxattr(self, path):		
	pathList = path.split('/')			
	fsNode = self.server.getData(path)
	if '/'+pathList[-1] in fsNode:
		attrs = fsNode['/'+pathList[-1]].get('attrs', {})
	else:		
        	attrs = fsNode.get('attrs', {})
	print 'listxattr executed..'
        return attrs.keys()
			
    def mkdir(self, path, mode):
	pathList = path.split('/')
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = self.server.getData(pathCwd)
	newDir = {}
	cwd[pathList[-1]] = {}
	newDir['/'+pathList[-1]] = dict(st_mode=(S_IFDIR | mode), st_nlink=2, st_size=0, st_ctime=time(), st_mtime=time(), st_atime=time())
	cwd['/'+pathList[-2]]['st_nlink'] += 1	
	self.server.putData(path, newDir)
	self.server.putData(pathCwd, cwd)
	print 'Path is', path 
	print 'mkdir executed..'

    def open(self, path, flags):		
        self.fd += 1
	print 'open executed..'
	print 'Return value', self.fd
        return self.fd

    def read(self, path, size, offset, fh):	
	print 'read executing..'
	dataRead = self.server.getData(path, 'data_server')
	print 'path is', path
	print 'offset is', offset
	print 'size', size
	print 'dataRead is', dataRead	
	print 'Return value', dataRead[offset:offset + size]
        return str(dataRead[offset:offset + size])

    def readdir(self, path, fh):
	print 'readdir executing..'
	print 'Path is ', path
	pathList = path.split('/')
	fsNode = self.server.getData(path)
	print ['.', '..'] + [x.split('/')[-1] for x in fsNode if x != '/'+pathList[-1]]
	for x in fsNode:
		print x 
        return ['.', '..'] + [x.split('/')[-1] for x in fsNode if x != '/'+pathList[-1]]

    def readlink(self, path):			
	print 'readlink executed..'
	dataRead = self.server.getData(path, 'data_server')
	print 'Return Value', dataRead
        return dataRead

    def removexattr(self, path, name):	
	pathList = path.split('/')
	fsNode = self.server.getData(path)
	if '/'+pathList[-1] in fsNode:
		attrs = fsNode['/'+pathList[-1]].get('attrs', {})
	else:		
        	attrs = fsNode.get('attrs', {})

        try:
	    print 'removexattr executing..'
            del attrs[name]
	    self.server.putData(path, fsNode)
        except KeyError:
	    print 'removexattr keyerror executed..'
            pass        # Should return ENOATTR

    def rename(self, old, new):	
        print 'rename executing..'
	print 'Old', old
	print 'New', new
	oldList = old.split('/')
	newList = new.split('/')
	if '/'.join(oldList[:-1]) == '':
		oldCwd = '/'
	else:
		oldCwd = '/'.join(oldList[:-1])
	if '/'.join(newList[:-1]) == '':
		newCwd = '/'
	else:
		newCwd = '/'.join(newList[:-1])
	cwdOld = self.server.getData(oldCwd)
	cwdNew = self.server.getData(newCwd)
	oldFileName = '/'+oldList[-2]+'/'+oldList[-1]
	newFileName = '/'+newList[-2]+'/'+newList[-1]
	if oldFileName in cwdOld:				#for moving files into directories or renaming files
		cwdNew[newFileName] = cwdOld.pop(oldFileName)
		if oldCwd == newCwd:
			cwdNew.pop(oldFileName)
		fileNode = self.server.getData(old)
		self.server.putData(new, fileNode)
		self.server.removeData(old)
		dataRead = self.server.getData(old, 'data_server')
		self.server.putData(new, dataRead, 'data_server')
		self.server.removeData(old, 'data_server')		
	else:
		cwdNew[newList[-1]] = {}			#for moving directories into directory or renaming directory
		dirNode = self.server.getData(old)
		dirInfo = dirNode.pop('/'+oldList[-1])
		newDirNode = dirNode
		newDirNode['/'+newList[-1]] = dirInfo
		for x in newDirNode:
			if(x.count('/') == 2):
				newDirNode['/'+newList[-1]+'/'+x.split('/')[-1]] = newDirNode.pop(x)
		cwdOld['/'+oldList[-2]]['st_nlink'] -= 1
		cwdNew['/'+newList[-2]]['st_nlink'] += 1
		self.movContent(dirNode, old, new)		#calls movData for modifying the path name of the files and then restoring their data content accordingly 
		self.server.putData(new, newDirNode)
		self.server.removeData(old)
		print 'newDirNode is', newDirNode
		if oldCwd == newCwd:
			cwdNew.pop(oldList[-1])		
		cwdOld.pop(oldList[-1])
	print 'cwdOld is', cwdOld
	print 'cwdNew is', cwdNew		
	self.server.putData(oldCwd, cwdOld)
	self.server.putData(newCwd, cwdNew)	

    def rmdir(self, path):
	print 'rmdir executing..'
	print 'Path is', path
	pathList = path.split('/')
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = self.server.getData(pathCwd)
        cwd.pop(pathList[-1])
 	cwd['/'+pathList[-2]]['st_nlink'] -= 1
	self.server.putData(pathCwd, cwd)
	self.server.removeData(path)

    def setxattr(self, path, name, value, options, position=0):	
        # Ignore options
	print 'setxattr executing..'
	pathList = path.split('/')
	fsNode = self.server.getData(path)
	if '/'+pathList[-1] in fsNode:
		attrs = fsNode['/'+pathList[-1]].setdefault('attrs', {})
	else:		
        	attrs = fsNode.setdefault('attrs', {}) 
        attrs[name] = value
	self.server.putData(path, fsNode)

    def statfs(self, path):		
	print 'statfs executed..'
        return dict(f_bsize=512, f_blocks=4096, f_bavail=2048)

    def symlink(self, target, source):	
	pathList = target.split('/')
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = self.server.getData(pathCwd)		
	fileName = '/'+pathList[-2]+'/'+pathList[-1]
	cwd[fileName] = ''
	newFile = dict(st_mode=(S_IFLNK | 0777), st_nlink=1, st_size=len(source))
	self.server.putData(pathCwd, cwd)
	self.server.putData(target, newFile)	
	self.server.putData(target, source, 'data_server')

    def truncate(self, path, length, fh=None):
	print 'truncate executing..'
	print 'Path is', path
	pathList = path.split('/')
	fileNode = self.server.getData(path)
	dataRead = self.server.getData(path, 'data_server')
        dataRead = dataRead[:length]
        fileNode['st_size'] = length
	self.server.putData(path, fileNode)
	self.server.putData(path, dataRead, 'data_server')

    def unlink(self, path):
	print 'unlink executing..'
	print 'Path is', path
	pathList = path.split('/')
	fileName = '/'+pathList[-2]+'/'+pathList[-1]
	if '/'.join(pathList[:-1]) == '':
		pathCwd = '/'
	else:
		pathCwd = '/'.join(pathList[:-1])
	cwd = self.server.getData(pathCwd)
        cwd.pop(fileName)
	self.server.putData(pathCwd, cwd)
	self.server.removeData(path)
	self.server.removeData(path, 'data_server')

    def utimens(self, path, times=None):
	print 'Path is', path
	pathList = path.split('/')
	fileNode = self.server.getData(path)
        now = time()
        atime, mtime = times if times else (now, now)
	print 'utimens executing..'
        fileNode['st_atime'] = atime
        fileNode['st_mtime'] = mtime
	self.server.putData(path, fileNode)

    def write(self, path, data, offset, fh):
	pathList = path.split('/')
	fileNode = self.server.getData(path)
	dataRead = self.server.getData(path, 'data_server')
	print 'path is', path
	print 'dataRead is', dataRead
	print 'dataRead[:offset] is', dataRead[:offset]
	print 'data is', data
        dataRead = dataRead[:offset] + data
	print 'dataRead now is', dataRead
        fileNode['st_size'] = len(dataRead)
	self.server.putData(path, fileNode)
	self.server.putData(path, dataRead, 'data_server')	
	print 'RETURN VALUE', len(data)
	print 'write executed..'
        return len(data)


if __name__ == '__main__':
    data_ports = []
    for i in argv[5:]:
       data_ports.append(i)

    meta_port = argv[4]
    Qr = argv[2]
    Qw = argv[3] 

    logging.getLogger().setLevel(logging.DEBUG)
    fuse = FUSE(Memory(Qr, Qw, meta_port, data_ports), argv[1], foreground=True, debug=True)

