#!/usr/bin/env python
"""
Author: David Wolinsky
Version: 0.02

Description:
The XmlRpc API for this library is:
  get(base64 key)
    Returns the value and ttl associated with the given key using a dictionary
      or an empty dictionary if there is no matching key
    Example usage:
      rv = rpc.get(Binary("key"))
      print rv => {"value": Binary, "ttl": 1000}
      print rv["value"].data => "value"
  put(base64 key, base64 value, int ttl)
    Inserts the key / value pair into the hashtable, using the same key will
      over-write existing values
    Example usage:  rpc.put(Binary("key"), Binary("value"), 1000)
  print_content()
    Print the contents of the HT
  read_file(string filename)
    Store the contents of the Hahelperable into a file
  write_file(string filename)
    Load the contents of the file into the Hahelperable
"""

import sys, SimpleXMLRPCServer, getopt, pickle, time, threading, xmlrpclib, unittest
from datetime import datetime, timedelta
from xmlrpclib import Binary

quit = 0;

# Presents a HT interface
class DataServer:
  def __init__(self, data_ports):
    self.data = {}
    self.other_ports = data_ports
    self.next_check = datetime.now() + timedelta(minutes = 5)
    self.synchronize()

  def synchronize(self):
    for port in self.other_ports:
      try:
        connection = xmlrpclib.ServerProxy("http://localhost:"+port+"/")
        if connection.count() != 0:
          key_list = connection.list_contents()
          for key in key_list:
            self.put(Binary(key), Binary(pickle.dumps(pickle.loads(connection.get(Binary(key))['value'].data))), 6000)
          print 'Server has synched itself'
          return;
      except:
        print 'Next server connection is not available yet'
        continue
    print 'This server is starting for the first time'
      

  def count(self):
    # Remove expired entries
    self.next_check = datetime.now() - timedelta(minutes = 5)
    self.check()
    return len(self.data)

  # Retrieve something from the HT
  def get(self, key):
    # Remove expired entries
    self.check()
    # Default return value
    rv = {}
    # If the key is in the data structure, return properly formated results
    key = key.data
    if key in self.data:
      ent = self.data[key]
      now = datetime.now()
      if ent[1] > now:
        ttl = (ent[1] - now).seconds
        rv = {"value": Binary(ent[0]), "ttl": ttl}
      else:
        del self.data[key]
    return rv

  # Insert something into the HT
  def put(self, key, value, ttl):
    # Remove expired entries
    self.check()
    end = datetime.now() + timedelta(seconds = ttl)
    self.data[key.data] = (value.data, end)
    return True
    
  def list_contents(self):
    key_list = []
    for keys in self.data:
      key_list.append(keys)
    return key_list 

  def corrupt(self, key):
    self.put(Binary(str(key)), Binary(pickle.dumps('Corrupted%$#%$Data\n')), 6000)
    return 'Corrupted%$#%$Data'

  def correctError(self, key):
    for port in self.other_ports:
      try:
        print 'Data correction taking place'
        connection = xmlrpclib.ServerProxy("http://localhost:"+port+"/")
        self.put(Binary(key), Binary(pickle.dumps(pickle.loads(connection.get(Binary(key))['value'].data))), 6000)
        return
      except:
        continue  
    return

  # Print the contents of the hashtable
  def print_content(self):
    print self.data
    return True

  # Remove expired entries
  def check(self):
    now = datetime.now()
    if self.next_check > now:
      return
    self.next_check = datetime.now() + timedelta(minutes = 5)
    to_remove = []
    for key, value in self.data.items():
      if value[1] < now:
        to_remove.append(key)
    for key in to_remove:
      del self.data[key]

  def terminate(self):
    global quit
    quit = 1
    return 1
       
def main():
  data_ports = []
  for i in sys.argv[2:]:
    data_ports.append(i)
  port = sys.argv[1]    

  serve(int(port), data_ports) 

# Start the xmlrpc server
def serve(port, data_ports):
  file_server = SimpleXMLRPCServer.SimpleXMLRPCServer(('', port))
  file_server.register_introspection_functions()
  ds = DataServer(data_ports)
  file_server.register_function(ds.get)
  file_server.register_function(ds.put)
  file_server.register_function(ds.print_content)
  file_server.register_function(ds.count)
  file_server.register_function(ds.list_contents)
  file_server.register_function(ds.corrupt)
  file_server.register_function(ds.correctError)
  file_server.register_function(ds.terminate)
  while not quit:
    file_server.handle_request()

# Execute the xmlrpc in a thread ... needed for testing
class serve_thread:
  def __call__(self, port):
    serve(port)


if __name__ == "__main__":
  main()

